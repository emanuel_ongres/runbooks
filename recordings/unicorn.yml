groups:
- name: Unicorn Transactions
  interval: 1m
  rules:
    # Request data, per controller/action.
    - record: controller_action:gitlab_transaction_duration_seconds_count:rate5m
      expr: sum(rate(gitlab_transaction_duration_seconds_count[5m])) without (fqdn, instance)

    - record: controller_action:gitlab_transaction_duration_seconds_sum:rate5m
      expr: sum(rate(gitlab_transaction_duration_seconds_sum[5m])) without (fqdn, instance)

    - record: controller_action:gitlab_transaction_duration_seconds_bucket:rate5m
      expr: sum(rate(gitlab_transaction_duration_seconds_bucket[5m])) without (fqdn, instance)

    - record: controller_action:gitlab_transaction_latency_seconds:rate5m
      expr: >
        controller_action:gitlab_transaction_duration_seconds_sum:rate5m
          /
        controller_action:gitlab_transaction_duration_seconds_count:rate5m

    - record: controller_action:gitlab_transaction_duration_seconds:p95
      expr: histogram_quantile(0.95, controller_action:gitlab_transaction_duration_seconds_bucket:rate5m)

    - record: controller_action:gitlab_transaction_duration_seconds:p99
      expr: histogram_quantile(0.99, controller_action:gitlab_transaction_duration_seconds_bucket:rate5m)

    # Request data, across the board.
    - record: job_environment:gitlab_transaction_duration_seconds_count:rate5m
      expr: sum(controller_action:gitlab_transaction_duration_seconds_count:rate5m) without (controller,action)

    - record: job_environment:gitlab_transaction_duration_seconds_sum:rate5m
      expr: sum(controller_action:gitlab_transaction_duration_seconds_sum:rate5m) without (controller,action)

    - record: job_environment:gitlab_transaction_duration_seconds_bucket:rate5m
      expr: sum(controller_action:gitlab_transaction_duration_seconds_bucket:rate5m) without (controller,action)

    - record: job_environment:gitlab_transaction_duration_latency_seconds:rate5m
      expr: >
        job_environment:gitlab_transaction_duration_seconds_sum:rate5m
          /
        job_environment:gitlab_transaction_duration_seconds_count:rate5m

    - record: job_environment:gitlab_transaction_duration_seconds:p95
      expr: histogram_quantile(0.95, job_environment:gitlab_transaction_duration_seconds_bucket:rate5m)

    - record: job_environment:gitlab_transaction_duration_seconds:p99
      expr: histogram_quantile(0.99, job_environment:gitlab_transaction_duration_seconds_bucket:rate5m)

- name: Unicorn SQL
  interval: 1m
  rules:
    # SQL data, per controller/action.
    - record: controller_action:gitlab_sql_duration_seconds_count:rate5m
      expr: sum(rate(gitlab_sql_duration_seconds_count[5m])) without (fqdn, instance)

    - record: controller_action:gitlab_sql_duration_seconds_sum:rate5m
      expr: sum(rate(gitlab_sql_duration_seconds_sum[5m])) without (fqdn, instance)

    - record: controller_action:gitlab_sql_duration_seconds_bucket:rate5m
      expr: sum(rate(gitlab_sql_duration_seconds_bucket[5m])) without (fqdn, instance)

    - record: controller_action:gitlab_sql_latency_seconds:rate5m
      expr: >
        controller_action:gitlab_sql_duration_seconds_sum:rate5m
          /
        controller_action:gitlab_sql_duration_seconds_count:rate5m

    - record: controller_action:gitlab_sql_duration_seconds:p95
      expr: histogram_quantile(0.95, controller_action:gitlab_sql_duration_seconds_bucket:rate5m)

    - record: controller_action:gitlab_sql_duration_seconds:p99
      expr: histogram_quantile(0.99, controller_action:gitlab_sql_duration_seconds_bucket:rate5m)

    # SQL data, across the board.
    - record: job_environment:gitlab_sql_duration_seconds_sum:rate5m
      expr: sum(controller_action:gitlab_sql_duration_seconds_sum:rate5m) without (controller, action)

    - record: job_environment:gitlab_sql_duration_seconds_count:rate5m
      expr: sum(controller_action:gitlab_sql_duration_seconds_count:rate5m) without (controller, action)

    - record: job_environment:gitlab_sql_duration_seconds_bucket:rate5m
      expr: sum(controller_action:gitlab_sql_duration_seconds_bucket:rate5m) without (controller,action)

    - record: job_environment:gitlab_sql_latency_seconds:rate5m
      expr: >
        job_environment:gitlab_sql_duration_seconds_sum:rate5m
          /
        job_environment:gitlab_sql_duration_seconds_count:rate5m

    - record: job_environment:gitlab_sql_duration_seconds:p95
      expr: histogram_quantile(0.95, job_environment:gitlab_sql_duration_seconds_bucket:rate5m)

    - record: job_environment:gitlab_sql_duration_seconds:p99
      expr: histogram_quantile(0.99, job_environment:gitlab_sql_duration_seconds_bucket:rate5m)

- name: Unicorn Gitaly
  interval: 1m
  rules:
    # Gitaly data, per controller/action.
    - record: controller_action:gitaly_duration_seconds_bucket:rate5m
      expr: sum(rate(gitaly_controller_action_duration_seconds_bucket[5m])) without(fqdn, instance, rpc, gitaly_service)

    - record: controller_action:gitaly_duration_seconds:p95
      expr: histogram_quantile(0.95, controller_action:gitaly_duration_seconds_bucket:rate5m)

    - record: controller_action:gitaly_duration_seconds:p99
      expr: histogram_quantile(0.99, controller_action:gitaly_duration_seconds_bucket:rate5m)

- name: Unicorn Cache
  interval: 1m
  rules:
    # Cache data, per controller/action.
    - record: controller_action:gitlab_cache_operation_duration_seconds_bucket:rate5m
      expr: sum(rate(gitlab_cache_operation_duration_seconds_bucket[5m])) without(fqdn, instance, operation)

    - record: controller_action:gitlab_cache_operation_duration_seconds:p95
      expr: histogram_quantile(0.95, controller_action:gitlab_cache_operation_duration_seconds_bucket:rate5m)

    - record: controller_action:gitlab_cache_operation_duration_seconds:p99
      expr: histogram_quantile(0.99, controller_action:gitlab_cache_operation_duration_seconds_bucket:rate5m)